<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title>Login</title>
    <link rel="stylesheet" href="/css/app.css">
    <script defer src="{{ asset('js/app.js') }}"></script>
    <meta name="csrf" value="{{ csrf_token() }">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
      rel="stylesheet">
</head>
<body>
    <header data-pageTitle="Login" id="header">
    </header>
    <main class="container" id="login">
    </main>
</body>

</html>