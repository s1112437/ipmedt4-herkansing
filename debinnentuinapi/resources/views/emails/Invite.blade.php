<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/app.css">
    <title>De Binnentuin</title>
</head>
<body>
    <article class= email>
        <h1 class="email__titel">{{$details['title']}}</h1>
        <p class="email__body">{{$details['body']}}</p>
        <br>
        <a class="email__link" href="http://localhost:3000/registerCommunity/{{$details['token']}}"
        style="text-decoration: none; background: #336633; color: white; padding: 10px; border-radius: 10px; font-weight: bold;"
         >{{$details['button']}}</a>
        <br>
        <br>
        <p class="email__body" >Bedankt, de Binnentuin</p>
        <figure>
        <img src="https://rubenjerry.nl/wp-content/uploads/2019/05/logo_green-300x67.png"/>
        </figure>
    </article>
</body>
</html>