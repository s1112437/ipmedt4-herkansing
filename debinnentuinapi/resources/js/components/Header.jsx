import React from 'react';
import ReactDOM from 'react-dom';

function Header(props) {
    return (
        <article className="header">
            <section className="material-icons header__back">
            arrow_back_ios
            </section>
            <h1 className="header__title">
                {props.pageTitle}
            </h1>
            <section>
            </section>
        </article>
    )
};
export default Header;

if (document.getElementById('header')) {
    ReactDOM.render(<Header pageTitle={document.getElementById('header').getAttribute('data-pageTitle')} />, document.getElementById('header'));
}