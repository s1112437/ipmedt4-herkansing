import React, { Component } from 'react' ;

export default class AantalPersonen extends React.Component{

        render(){
        return (
            <section className="reserveren__persons">
                <button onClick={this.props.onRemovePerson} className="reserveren__persons__button material-icons">remove</button>
                <input value={this.props.aantalPersonen} className="reserveren__persons__input" type="number">
                </input>
                <button onClick={this.props.onAddPerson} className="reserveren__persons__button material-icons">add</button>
            </section>
        )
        }
};