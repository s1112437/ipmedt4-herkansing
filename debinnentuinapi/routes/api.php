<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Foundation\Auth\ResetsPasswords;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/locations', "LocationController@index");
Route::patch('/timeslots', "TijdslotenController@editTimeslot");
Route::post('/reserveringen', 'ReserveringenController@store');
Route::get('/reserveringen/{startTijd}/{date}/{locationid}', 'ReserveringenController@getReserveringenByTime');
Route::get('/reserveringen/{datetime}', 'ReserveringenController@getReserveringenByTimeslot');
Route::get('/reserveringenbytable/{tableId}/{datetime}', 'ReserveringenController@getReserveringenByTable');
Route::post('/maps', 'MapsController@store');
Route::get('/maps/{locationId}', 'MapsController@show');
Route::get('/timeslots/{location?}/{date?}/{persons?}/{available?}', "TijdslotenController@get");
Route::get('/timeslot/{tijdSlotId}', "TijdslotenController@show");
Route::get('/reservering/{reserveringId}', "ReserveringenController@show");
Route::post('/reserveringsettable/{reserveringId}/{tableId}', "ReserveringenController@setTable");

Route::get('/bestellingen/getorderid', "BestellingenController@getOrderId");
Route::get('/bestellingenbyreservering/{reserveringId}', "BestellingOrdersController@getByReservering");

Route::get('/bestellingen/getprice', "BestellingenController@getPrice");
Route::get('/pay/{orderId}/{amount}', 'MollieHandler@preparePayment');

Route::get('/tables/{tableId}', "TablesController@show");
Route::get('/availableTables/{tijdslotId}/{locationId}', "TablesController@getAvailableTables");

Route::get('/admin/timeslots/{date?}/{location?}', "TijdslotenController@getTimeslotsByDate");
Route::get('/tablesbylocation/{locationId}', "TablesController@index");
Route::post('/tables', "TablesController@store");

Route::get('/checkAvailability/{tijdslotId}/{amountPersons}', "TijdslotenController@checkAvailability");


Route::get('/location/{locationId}', "LocationController@show");
Route::get('/roosters', "RoostersController@index");
Route::get('/admin/rooster/{roosterId}/edit', 'RoostersController@edit');
Route::get('/rooster/{roosterId}', 'RoostersController@show');
Route::post('/rooster', 'RoostersController@store');
Route::post('/rooster/{id}', 'RoostersController@save');
Route::delete('/rooster/{roosterId}', 'RoostersController@destroy');
Route::get('/userklant/{userId}','UserController@showWithKlant');
Route::get('/user/{userId}','UserController@show');

Route::patch('/user/{userId}','UserController@update');

Route::get('/klant/{klantId}','KlantenController@show');
Route::post('/klant','KlantenController@store');
Route::get('/klantReserveringen/{klantId}','KlantenController@getReserveringen');

Route::get('/klanten','KlantenController@getKlant');

Route::get('/openingstijden/{roosterId}', 'OpeningstijdenController@getOpeningstijden');
Route::get('/openingstijdenbylocation/{location}/{day}', 'OpeningstijdenController@getOpeningstijdenByLocation');
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('loginadmin', 'AuthController@loginAdmin');

    Route::post('register/{token?}', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('user-profile', 'AuthController@userProfile');
    Route::get('check-authenticate', 'AuthController@isAuthenticated');
});
Route::post('/inviteSend', 'CommunityInvitesController@sendInvite');
Route::get('/invitesRooster', 'CommunityInvitesController@getCommunityInvite');

Route::post('/send-mail', 'PasswordResetController@sendEmail');
Route::post('/resetWachtwoord', 'PasswordResetController@resetWachtwoord');
Route::get('/checkvalid/{token}', 'PasswordResetController@checkValid');
Route::patch('/veranderWachtwoord', 'PasswordResetController@veranderWachtwoord');

Route::post('/koppelklantreservering', 'KoppelKlantReserveringController@store');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/bestellen/{categorie}', 'MenuController@show');
Route::post('/bestellen', 'BestellingenController@store');

Route::get('/bestellen/orders/{bestellingid}', 'BestellingOrdersController@getOrders');
Route::get('/bestelorders', 'BestellingOrdersController@getAllOrders');