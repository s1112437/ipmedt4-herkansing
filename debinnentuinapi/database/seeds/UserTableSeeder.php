<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=> 'Jerry',
            'email'=> 'debinnentuin@plnt.nl',
            'password'=> Hash::make("Welkom01"),
            'role' => 'Admin'
        ]);
    }
}
