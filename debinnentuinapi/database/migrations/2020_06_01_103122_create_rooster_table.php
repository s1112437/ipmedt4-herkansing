<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoosterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooster', function (Blueprint $table) {
            $table->id();
            $table->String('name');
            $table->unsignedBigInteger('location_id');
            $table->date('startDatum')->nullable();
            $table->date('eindDatum')->nullable();
            $table->boolean('oneindig')->default(0);
            $table->integer('aantalPersonen');
            $table->time('duurTijdSlot');
            $table->time('duurTussenTijdSlot');

            $table->foreign('location_id')->references('id')->on('location');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rooster', function (Blueprint $table) {
            $table->dropForeign('rooster_location_id_foreign');
        });
        Schema::dropIfExists('rooster');
    }
}
