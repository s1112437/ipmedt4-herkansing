<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\TijdSloten;
use App\Location;
use App\Dagen;
use App\Rooster;
use DateTime;

class TijdslotenController extends Controller
{
    public function get($location = null, $date = null, $persons = null, $available = null){
        $roosterIds = Location::find($location)->roosters()->pluck('id');
        // var_dump($roosterIds);
        $dag = Dagen::Where('Datum','=', date("Y-m-d" , strtotime($date)))->first();
        $tijdSloten = $dag->tijdsloten()->whereIn('rooster_id', $roosterIds)->get();
        return $tijdSloten;

    }
    public function getTimeslotsByDate($date, $location){
        $dag = Dagen::Where('Datum','=', date("Y-m-d" , strtotime($date)))->pluck('id');
        // $tijdsloten = Rooster::Where("location_id","=",$location)->whereHas('tijdsloten', function(Builder $query) use ($dag){
        //     $query->where('dagen_id','=', $dag);
        // })->get();
        
        $tijdsloten = TijdSloten::whereHas("rooster", function(Builder $query) use ($location){
            $query->where('rooster.location_id','=', $location);
        })->where('dagen_id','=', $dag)->get();
        // $dag = Dagen::Where('Datum','=', date("Y-m-d" , strtotime($date)))->pluck('id');
        return $tijdsloten;
    }
    public function editTimeslot(Request $request){
        $tijdslot = Tijdsloten::find($request->id);
        if(isset($request->beschikbaar)){
            $tijdslot->beschikbaar = $request->beschikbaar;

        }
        else if(isset($request->aantalPersonen)){
            $tijdslot->aantalPersonen = $request->aantalPersonen;
        }
        $tijdslot->save();
    }

    public function checkAvailability($tijdslotId, $amountPersons){
        date_default_timezone_set("Europe/Amsterdam");
        $tijdslot = TijdSloten::find($tijdslotId);
        $dag = $tijdslot->Dagen()->pluck('Datum')->first();
        if(new DateTime() > new DateTime($dag . $tijdslot->startTijd)){
            return response()->json([
                "Available" => false,
                "msg" => "Gekozen tijd is niet meer beschikbaar"
            ]);
        }
        else if($tijdslot->aantalPersonen < $amountPersons){
            return response()->json([
                "Available" => false,
                "msg" => "Gekozen tijd is niet meer beschikbaar voor het aantal gekozen personen"
            ]);
        }
        else{
            return response()->json([
                "Available" => true,
                "msg" => null
            ]);
        }
    }
    public function show($tijdSlotId){
        return response()->json(["Tijdslot" => TijdSloten::find($tijdSlotId), "Rooster" => "http://localhost:8000/api/rooster/" . TijdSloten::find($tijdSlotId)->Rooster()->first()->id]);
    }
}
