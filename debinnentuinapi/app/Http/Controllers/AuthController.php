<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Klanten;
use App\CommunityInvites;
use DateInterval;
use DateTime;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Resources\AuthResource;
use Illuminate\Http\JsonResponse;


class AuthController extends Controller
{
     /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }
     /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    // public function login(Request $request){
    // 	$validator = Validator::make($request->all(), [
    //         'email' => 'required|email',
    //         'password' => 'required|string|min:6',
    //     ]);

    //     if ($validator->fails()) {
    //         return response()->json($validator->errors(), 422);
    //     }

    //     if (! $token = auth()->attempt($validator->validated())) {
    //         return response()->json(['error' => 'Unauthorized'], 401);
    //     }

    //     return $this->createNewToken($token);
    // }
    public function login(LoginRequest $request): JsonResponse
    {
        return (new AuthResource(auth()->user()))
            ->response()
            ->withCookie(
                'token', 
                auth()->getToken()->get(), 
                config('jwt.ttl'), 
                '/'
            );
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request, $token = null) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);
        $klantenValidator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:klanten',
            'phone' => 'required|phone:AUTO,NL,mobile'
        ]);

        if($validator->fails() || $klantenValidator->fails()){
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 400);
            }
            else{
                return response()->json($klantenValidator->errors()->toJson(), 400);
            }
        }
        if($token){
            $communityInvite = CommunityInvites::find(CommunityInvites::where('token','=', $token)->first()->id);
            $created_at = new DateTime($communityInvite->created_at);
            $validuntil = $created_at->add(new DateInterval('PT' . 1440 . 'M')); /* 1 dag tijd */
            if(new DateTime() < $validuntil &&  !$communityInvite->verified){
                $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));
    
                $klanten = Klanten::create(array_merge(
                    $klantenValidator->validated()
                ));
                $klanten->users_id = $user->id;
                $klanten->community_lid = true;
                $communityInvite->verified = true;
                $communityInvite->save();
                $klanten->save();
            }
            else {
                $user = null;
                $klanten = null;
                return response()->json([
                    'error'=> true,
                    'message'=> "Token is niet meer geldig"
                ], 400);
            }
            
        }
        else if (! isset($token)){
            $user = User::create(array_merge(
                $validator->validated(),
                ['password' => bcrypt($request->password)]
            ));

            $klanten = Klanten::create(array_merge(
                $klantenValidator->validated()
            ));
            $klanten->users_id = $user->id;
            $klanten->save();
        }
        

        return response()->json([
            'message' => 'User successfully registered',
            'useridmessage' => $user->id,
            'user' => $user,
            'klant'=> $klanten->id
            
        ], 201);
    }

     /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }
    public function isAuthenticated(){
        if(auth()->user()){
            return response()->json([
                "Authenticated" => true,
                "user" => auth()->user()
            ]);
        }
        else {
            abort(401);
        }
    }

}
