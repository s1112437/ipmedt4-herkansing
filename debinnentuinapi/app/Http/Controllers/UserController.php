<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Klanten;

class UserController extends Controller
{
    public function showWithKlant($userId){
        return response()->json(["User" => User::find($userId), "Klant" => "http://localhost:8000/api/klant/" . Klanten::where('users_id','=',$userId)->first()->id]);
    }
    public function show($userId){
        return response()->json(["User" => User::find($userId)]);
    }

    public function update(Request $request){
        $user = User::find($request->userId);
        $klant = Klanten::where('users_id','=', $user->id)->first();
        if($request->name){
            $user->name = $request->name;
            $klant->name = $request->name;
        }
        if($request->email){
            $user->email = $request->email;
            $klant->email = $request->email;
        }
        if($request->phone){
            $klant->phone = $request->phone;
        }
        $user->save();
        $klant->save();

    }
}
