<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DateTime;
use DateInterval;
use App\Mail\SendInvite;
use App\User;
use App\CommunityInvites;


class CommunityInvitesController extends Controller
{
    public function sendInvite(Request $request){

        $communityInvite = new CommunityInvites();
        $communityInvite->token = Str::random(60);
        $communityInvite->email = $request->email;
        echo($communityInvite->token);
        $communityInvite->save();
        $details = [
            'title'  => 'Community Member',
            'body'   => 'U bent een community klant van de de Binnentuin. Hiermee staat u open voor een korting, klik op de onderstaande knop om u te registreren.',
            'button' => 'Registreren',
            'token'  => $communityInvite->token
        ];
        Mail::to($request->email)->send(new \App\Mail\SendInvite($details));
        return response()->json([
            "error" => false, 
            "message" => "Er is een community invite mail naar " . $request->email . " gestuurd."
        ]);


    }

    public function getCommunityInvite()
    {
        $communityMail = CommunityInvites::where('email', '!=', null)->get();
        return $communityMail;
    }

    // $validuntil = $created_at->add(new DateInterval('PT' . 1440 . 'M')); /* dag geldig */
}
