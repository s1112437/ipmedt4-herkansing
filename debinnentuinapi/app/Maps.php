<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Maps extends Model
{
    protected $table = 'maps';
    public $timestamps = false;

    public function Location(){
        return $this->belongsTo('App\Location');
    }
}
