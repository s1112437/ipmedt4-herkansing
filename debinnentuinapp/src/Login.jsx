import React from 'react';
import {connect} from "react-redux";
import { withRouter } from "react-router";
import {setHeaderTitle, setAuthenticated, setCustomer} from "./Redux/Actions";
import AXIOS from "./axios";
import {
    useHistory,
    useLocation,
    Redirect
  } from "react-router-dom";

class Login extends React.Component{
    state = {isLoggedIn: false, error: '', user:{email:'', password:''}}


    componentDidMount(){
        // console.log(this)
        // console.log(this.props.location)
        this.props.setHeaderTitle("Login")

    }
    
    handleEmail = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, email: value
            }
        })); 
    }

    handlePassword = (e) => {
        let value= e.target.value;
        this.setState(prevState => ({
            user: {
                ...prevState.user, password: value
            }
        })); 
    }
    getKlant(klant) {
        AXIOS.get(klant).then((res) => {
            this.setCustomer(res.data);
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        console.log("JAAAAAAAA")

        let userData = this.state.user;
        // axios.defaults.headers.common['CSRF-Token'] = "my CSRF-Token"
        AXIOS.post("http://localhost:8000/api/auth/login", {
            email: this.state.user.email,
            password: this.state.user.password,
           
            }).then(response => {
            return response;
            

        }).then(json => {
            console.log(json)
            if (json.status === 200){
                AXIOS.defaults.headers.common['CSRF-Token'] = json.data.csrf_token;

                AXIOS.get("http://localhost:8000/api/userklant/" + json.data.data.id).then(res => {
                    AXIOS.get(res.data.Klant).then(res => {
                        this.props.setCustomer(res.data);
                    })
                });
                this.props.setAuthenticated(true)
                
            }
        }).catch(error => {if (error.response) {
            // The request was made and the server responded with a status code that falls out of the range of 2xx
            let err = error.response.data;
            this.setState({
              error: err.message,
              errorMessage: err.errors,
              formSubmitting: false
            })
          }
          else if (error.request) {
            // The request was made but no response was received `error.request` is an instance of XMLHttpRequest in the browser and an instance of http.ClientRequest in node.js
            let err = error.request;
            this.setState({
              error: err,
              formSubmitting: false
            })
         } else {
           // Something happened in setting up the request that triggered an Error
           let err = error.message;
           this.setState({
             error: err,
             formSubmitting: false
           })
        //    console.log(this.state.error)
       }
     }).finally(this.setState({error: ''}));
    }

    handleClick = (path) =>{
        this.props.history.push(path)
    } 


    render(){
        if (this.props.isAuthenticated) {
            const { from } = this.props.location.state || { from: { pathname: '/home' } }
            return <Redirect customer={this.props.customer} to={from} />
          }
        return(
            <article className ="container login">
                {setHeaderTitle}
                <form className="login__form" onSubmit={this.handleSubmit}>
                    
                    <label className="login__form__label" name="email">E-mail</label>
                    <input onChange={this.handleEmail} className="login__form__input" name="email" type="email" placeholder="Vul E-mail in" required/>
                    <label className="login__form__label" name="password">Wachtwoord</label>
                    <input onChange={this.handlePassword} className="login__form__input" pattern=".{6,}" name="password" type="password" placeholder="Vul wachtwoord in" required/>
                    <label className="login__form__label">
                        <input type="checkbox" defaultChecked="" name="remember" /> Remember me
                    </label>        
                    <button className="login__form__button" type="submit" value="Submit">Login</button>
                    <a  onClick={() => this.handleClick("/register")} className="login__form__link">Register hier</a>
                    <a  onClick={() => this.handleClick("/wachtwoordHerstel")} className="login__form__link">Wachtwoord vergeten?</a>
                </form>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle,
             isAuthenticated: state.isAuthenticated,
            customer: state.customer};
}
export default withRouter(connect(mapStateToProps,{setHeaderTitle: setHeaderTitle, setAuthenticated: setAuthenticated, setCustomer: setCustomer}) (Login));