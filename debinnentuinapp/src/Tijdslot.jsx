import React from 'react';


export default class Tijdslot extends React.Component {
    selectTijdslot = (tijdSlotId) => {
        this.props.selectTijdslot(tijdSlotId)
    }
    render(){
    return (
        <button onClick={() => this.selectTijdslot(this.props.tijdslotId)} className={this.props.styling}>
        {this.props.startTijd + ' - ' + this.props.eindTijd}
        </button>
    )
    }
}