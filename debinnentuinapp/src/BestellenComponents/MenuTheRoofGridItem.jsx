import React from 'react';
import { connect } from 'react-redux';
import { setCartItemOrders, setModalDisplayOn, getCategoryMenuData, setMenuObjectQuantity, setCartAmount} from "../Redux/Actions";

class MenuTheRoofGridItem extends React.Component{
    IncrementItem = () => {
        let id = this.props.menu_item_id;
        let menuArrayData = this.props.cartItem;
        if(menuArrayData.length > 0){
            for(let i = 0; i<menuArrayData.length; i++){
                if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] < 9){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] + 1
                    }); 
                    return;
                }
            }
            
            this.props.setMenuObjectQuantity({
                menu_id: id,
                menu_item: this.props.title,
                menu_item_prijs: this.props.prize,
                quantity: 1
            }); 
            this.props.setCartAmount(this.props.cartItem.length+1);
            return;
        }
        else{
            this.props.setMenuObjectQuantity({
                menu_id: id,
                menu_item: this.props.title,
                menu_item_prijs: this.props.prize,
                quantity: 1
            });
            this.props.setCartAmount(this.props.cartItem.length+1);
        }
    }

    DecreaseItem = () => {
        let id = this.props.menu_item_id;
        let menuArrayData = this.props.cartItem;
        if(menuArrayData.length > 0){
            for(let i = 0; i<menuArrayData.length; i++){
                if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] === 1){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] - 1
                    });
                    this.props.setCartAmount(this.props.cartItem.length-1);
                    return;
                }
                
                else if(menuArrayData[i]["menu_id"] === id & menuArrayData[i]["quantity"] !== 0){
                    this.props.setMenuObjectQuantity({
                        menu_id: id,
                        menu_item: this.props.title,
                        menu_item_prijs: this.props.prize,
                        quantity: menuArrayData[i]["quantity"] - 1
                    });
                }
                
            }
        }
    }

    setObjectQuantity = (data, menu_id) =>{
        if(data !==[]){
            for(let i=0; i<data.length; i++){
                if(data[i]["menu_id"] === menu_id){
                    return data[i]["quantity"];
                }
            }
        }
        return 0;
    }
    render(){
        return(
            <section className="bestellen__theroof__c2__griditem">
                <section className="bestellen__theroof__c2__griditem__top">{this.props.title}</section>
                <p className="bestellen__theroof__c2__griditem__description">{this.props.description}</p>
                <div className="bestellen__theroof__c2__griditem__box button_item">
                    <input type="button" value="-" onClick = {this.DecreaseItem} className="minus"/>

                    <input readOnly type="number" name="quantity" value={this.setObjectQuantity(this.props.cartItem,this.props.menu_item_id)} className="input-number qtyicon"/>
                    
                    <input type="button" value="+" onClick = {this.IncrementItem} className="plus" />
                </div>
            </section>
        );
    }
}

const mapStateToProps = state =>{
    return { cartItem: state.cartItem, object_quantity: state.object_quantity };
}

export default connect(
    mapStateToProps,
    {   setCartItemOrders: setCartItemOrders,
        setModalDisplayOn:setModalDisplayOn,
        getCategoryMenuData :getCategoryMenuData,
        setMenuObjectQuantity: setMenuObjectQuantity,
        setCartAmount: setCartAmount }
)(MenuTheRoofGridItem);