import React from 'react';
import { connect } from 'react-redux';

class BestellenBevestigingsButton extends React.Component{

    redirectToBestelOverzicht = () =>{
        this.props.history.push("/besteloverzicht");
    }


    checkEmptyCartForStyle = () =>{
        if(this.props.cartItem.length >= 1){
            return "bevestiging--button"
        }
        else{
            return "bevestiging--button --bevestigingsbutton__inactive"
        }
    }

    render(){
        //console.log(this.getOrdersFromId());
        return(
            <section className="container bestellen__bottom">
                <button className={this.checkEmptyCartForStyle()}  onClick = {this.redirectToBestelOverzicht}>
                    <h4 className="bevestiging--button__title">Naar Besteloverzicht</h4>
                    <p className="bevestiging--button__description">En controleer</p>
                </button>
            </section>
        );
    }
}

const mapStateToProps = state =>{
    return { cartItem: state.cartItem, finalOrder: state.finalOrder };
}


export default connect(
    mapStateToProps)(BestellenBevestigingsButton);
    
