import React from 'react';
import axios from 'axios';

import { connect } from 'react-redux';
import { setModalDisplayOn, getCategoryMenuData, setCartDisplayStyle } from "../Redux/Actions";

class MenuCategorie extends React.Component{
    onMenuCategoryClicked = () =>{  
        this.makeMenuApiCall(this.props.id);
        this.props.setCartDisplayStyle("none");
        this.props.setModalDisplayOn();
    }

    makeMenuApiCall = (menu_id) =>{
        if(menu_id != null){
            const BASE_URL = "http://localhost:8000/api/bestellen/";
            axios.get(BASE_URL + menu_id).then(res=>{
                this.props.getCategoryMenuData(res.data);
            });
        };
    }
    
    setBackgroundImageForButtons(props) {
        switch(props) {
                case 'Snacks':
                    return props = {backgroundImage: 'url(https://i.imgur.com/VanQUDE.jpg'};
                case 'Toasts':
                    return props = {backgroundImage: 'url(https://i.imgur.com/KlP3QAt.jpg'};
                case 'Salads':
                    return props = {backgroundImage: 'url(https://i.imgur.com/LtgeCZG.jpg'};
                case 'Sandwiches':
                    return props = {backgroundImage: 'url(https://i.imgur.com/63UjiWZ.jpg'};
                case 'Coffee':
                    return props = {backgroundImage: 'url(https://i.imgur.com/N0yAxrx.jpg'};
                case 'Hot Drinks':
                    return props = {backgroundImage: 'url(https://i.imgur.com/xSZ7Jj4.jpg'};
                case 'Cold Drinks':
                    return props = {backgroundImage: 'url(https://i.imgur.com/vyPWfW0.jpg'};
                default:
                    return;
        }
    }

    render(){
        return(
            <button onClick={this.onMenuCategoryClicked} className="bestellen__menucategorie" style={this.setBackgroundImageForButtons(this.props.title)}>
                <div className="bestellen__menucategorie__shadow"></div>
                <h2 className="bestellen__menucategorie__title">{this.props.title}</h2>
            </button>
        );
    }
}

const mapStateToProps = state =>{
    return { menu_data: state.menu_data };
}
export default connect(
    mapStateToProps,
    { setModalDisplayOn: setModalDisplayOn, getCategoryMenuData: getCategoryMenuData, setCartDisplayStyle: setCartDisplayStyle}
)(MenuCategorie);
