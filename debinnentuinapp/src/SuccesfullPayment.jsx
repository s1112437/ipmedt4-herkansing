import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import PayLogo from "./betaalComponents/PayLogo";
import SuccesMessage from "./betaalComponents/SuccesMessage";
import SuccesRedirect from "./betaalComponents/SuccesRedirect";

class SuccesfullPayment extends React.Component{
   componentDidMount(){
        this.props.setHeaderTitle("Bedankt voor uw bestelling!")
    }

   render(){
      return(
        <article className="container betaling">
          <PayLogo />
          <h1 className="betaling__header">Bedankt voor uw bestelling!</h1>
          <SuccesMessage />
          <SuccesRedirect />
        </article>
      )
   }

}

const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (SuccesfullPayment);
