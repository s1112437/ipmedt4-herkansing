import React from 'react';
class SuccesMessage extends React.Component{


    render(){
        return (
          <section className="betaling__succes">
            <p className="betaling__succes__text">Uw betaling is geslaagd dus wij van De Binnentuin staan klaar om u te verwelkomen als gast bij één van onze locaties. U bent welkom op de door u gereserveerde tijd. Wij zorgen dan dat uw bestelling klaar is. Tot dan!</p>
          </section>
        );
    }

}

export default SuccesMessage;
