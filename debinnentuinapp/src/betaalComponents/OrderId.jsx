import React from 'react';
import axios from 'axios';
class OrderId extends React.Component{

  state = {
    orderid: ""
  }

  getOrderID = () => {
    if(this.state.orderid == ""){
      const BASE_URL = "http://localhost:8000/api/bestellingen/getorderid";
      axios.get(BASE_URL).then(res=>{
          console.log(res.data);
          this.setState({orderid: res.data})
      });
    }
  }

    render(){
        return (
          <section className="betaling__orderid">
           <p className="betaling__orderid__text">Order nummer:</p>
           <p className="betaling__orderid__ordernr" id="js--orderid">#{this.getOrderID()}
           {this.state.orderid}</p>
          </section>
        );
    }

}

export default OrderId;
