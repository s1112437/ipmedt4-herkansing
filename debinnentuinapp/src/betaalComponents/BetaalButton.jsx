import React from 'react';
import axios from 'axios';
import TotalPrice from "./TotalPrice";
import OrderId from "./OrderId";
class BetaalButton extends React.Component{

    getPrice = () => {
      const priceString = document.getElementById("js--price").innerText;
      const priceFloat = parseFloat(priceString.substr(2));
      const finalString = priceFloat.toFixed(2).toString();
      return finalString;
    }

    getOrderId = () => {
      const orderString = document.getElementById("js--orderid").innerText;
      const orderid = orderString.substr(1);
      return orderid;
    }

    Betaling = () => {
      const BASE_URL = "http://localhost:8000/api/pay/" + this.getOrderId() + '/' + this.getPrice();
      axios.get(BASE_URL).then(res=>{
          console.log(res.data);
          window.location.href = res.data;
      });
    }



    render(){
        return (
          <button className="betaling__button" onClick={this.Betaling}>
            Betalen
          </button>
        );
    }

}

export default BetaalButton;
