import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle, setAuthenticated, setCustomer} from "./Redux/Actions";
import AXIOS from "./axios";
// AXIOS.get(res.data.Klant).then(res => {
//     this.setState({
//         customer: res.data
//     })
// })
class Profiel extends React.Component{
    state = {email: null, name: null, phone: null, viewMode: true, editMode: false}
    componentDidMount(){
       
        this.getCustomer();
       this.props.setHeaderTitle("Profiel");

        
    }
    handleClick = (path) =>{
        this.props.history.push(path)
    } 
    changeField = (e) => {
        let target = e.target
        let name = target.name
        let value = target.value
        this.setState({
            [name] : value
        })
    }
    logout = () => {

          AXIOS.post("http://localhost:8000/api/auth/logout").then((res) => {
              this.props.setAuthenticated(false)
            this.props.history.replace("/login")
        })
    }
    enableEditMode = () => {
        this.setState({
            viewMode: false,
            editMode: true
        })
    }
    save = () => {
        AXIOS.patch("http://localhost:8000/api/user/" + this.state.userId, this.state).then(res => {
            this.setState({
                viewMode: true,
                editMode: false
            })
        })
    }
    getCustomer = () => {
        AXIOS.get("http://localhost:8000/api/auth/" + 'check-authenticate').then((res) => {
            if(res.status === 200){
                this.setState({
                    userId: res.data.user.id
                })
                AXIOS.get("http://localhost:8000/api/userklant/" + res.data.user.id).then(res => {
                    AXIOS.get(res.data.Klant).then(res => {
                        this.setState({
                            email: res.data.email,
                            name: res.data.name,
                            phone: res.data.phone
                        })
                    })
                });
            }
        }).catch(error => {if (error.response) {
            console.log("Unauthorized")
        }
    })
    console.log(this.mapStateToProps)
    }
    render(){
        
        return(
            <article className ="container profiel">
                {setHeaderTitle}

                <button hidden={this.state.editMode} className="profiel__wijzigen" onClick={() => this.enableEditMode()}>Profiel wijzigen</button>
                <button hidden={this.state.viewMode} className="profiel__wijzigen" onClick={() => this.save()}>Profiel Opslaan</button>

                <form className="profiel__form">
                    <header hidden={this.state.editMode} className="profiel__form__header">
                        <h2>{this.state.name}</h2>
                    </header>
                    <input hidden={this.state.viewMode}   className="profiel__form__input" name="name" onChange={this.changeField} value={this.state.name}/>
                    <section className="profiel__form__imgcontainer" >
                        <figure>
                        <img className="profiel__form__imgcontainer__profielfoto" src="../img/profieldefaultImg.png" alt="profile picture" />
                        </figure>
                        <div hidden={this.state.viewMode} className="profiel__form__imgcontainer__icons material-icons" onClick={() => this.handleClick("")}>add_a_photo</div> 
                    </section>
                    <input disabled={this.state.viewMode} className="profiel__form__input" onChange={this.changeField} name="email" value={this.state.email} />
                    <input disabled={this.state.viewMode} className="profiel__form__input" onChange={this.changeField} name="phone" value={this.state.phone} />
                    <button hidden={this.state.editMode} className="profiel__form__button" name="reserveringen" onClick={() => this.handleClick("/profielReserveringen")}>Vorige reserveringen</button> 
                    <button hidden={this.state.editMode} className="profiel__form__button" name="password" onClick={() => this.handleClick("/WachtwoordWijzigen")}>Wachtwoord veranderen</button>     

                    <a className="profiel__form__logout"onClick={this.logout}>Uitloggen</a>  
                </form>

            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle, customer: state.customer };
}

export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle, setAuthenticated: setAuthenticated, setCustomer: setCustomer}) (Profiel);

