import React from "react";

import BestellenCartOverzicht from './BestellenComponents/BestellenCartOverzicht';

import { connect } from 'react-redux';
import { setHeaderTitle, storeFinalOrders, setTextAreaContent} from "./Redux/Actions";
import axios from 'axios';

import "./style/app.css";

import BestelOverzichtObject from './BestelOverzichtComponents/BestelOverzichtObject';

class BestellenOverzicht extends React.Component{
    componentDidMount(){
        this.props.setHeaderTitle("Besteloverzicht");
    }
    storeOrdersInTable = () =>{
        var params = [this.props.cartItem, this.props.textArea, this.getTotalPrize(this.props.cartItem), this.props.reservering];
        
        axios.post('http://localhost:8000/api/bestellen', params).then(response => {
            this.props.storeFinalOrders(response.data);
            //this.getOrdersFromId(this.props.finalOrder[0]['bestelling_id']);
            console.log(this.props.finalOrder);

            // redirect door Gijs om Redux in bestelvalidatie te testen!
            this.props.history.push('bestelvalidatie');
            // Dit kan dus weg of veranderd worden ^
          });;
    }

    getOrdersFromId = (id ) =>{
        axios.get('http://localhost:8000/api/bestellen/orders/' + id ).then(response=>{
                console.log(response.data);
                return response.data;
            })
    }

    displayOverzichtContent = (data)=>{
        if(data !== ""){
            let list = []
            for(let i = 0; i<data.length;i++){
                list.push(
                <BestelOverzichtObject
                    title={data[i].menu_item}
                    menu_item_id={data[i].menu_id}
                    description={data[i].description}
                    prize={data[i].menu_item_prijs}
                    key={data[i].menu_id}
                    history={this.props.history}
                    />
                )
            }
            return list;
        }
    }
    
    getTotalPrize = (arrayCart) =>{
        let totaal = 0;
        if(arrayCart !== []){
            for(let i = 0; i<arrayCart.length;i++){
                totaal += arrayCart[i]['quantity'] * arrayCart[i]['menu_item_prijs']
            }
            return totaal.toFixed(2);
        }
        return 0;
    }

    textAreaChanged = (event) =>{
        this.props.setTextAreaContent(event.target.value);
    }
    
    checkEmptyCartForStyle = () =>{
        if(this.props.cartItem.length >= 1){
            return "bestellenOverzicht__buttonContainer__buttonBestel"
        }
        else{
            return "bestellenOverzicht__buttonContainer__buttonBestel --inactiveButton"
        }
    }

    render(){
        return(
            <article className="container bestellenOverzicht">
                <BestellenCartOverzicht history={this.props.history}/>
                <section className="bestellenOverzicht__itemsContainer">
                    {this.displayOverzichtContent(this.props.cartItem)}
                </section>
                
            <form className="bestellenOverzicht__form">
                <label className="bestellenOverzicht__form__label" name="formExtraInfo">Allergiën/Dieet(en)</label>
                <textarea className="bestellenOverzicht__form__inputfield" onChange={this.textAreaChanged.bind(this)} type="text" name="formExtraInfo"></textarea>
            </form>

            <h2 className="bestellenOverzicht__totaal">Totaal: {this.getTotalPrize(this.props.cartItem)} &euro;</h2>

            <section className="bestellenOverzicht__buttonContainer">
                <button className={this.checkEmptyCartForStyle()} onClick={this.storeOrdersInTable}>Betalen</button>  
            </section>     
                
            </article>
        );
    }
}

const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle, finalOrder: state.finalOrder, cartItem: state.cartItem, textArea: state.textArea, reservering: state.reservering};
}
export default connect(mapStateToProps,
    {setHeaderTitle: setHeaderTitle,
        storeFinalOrders: storeFinalOrders
        , setTextAreaContent: setTextAreaContent})(BestellenOverzicht);