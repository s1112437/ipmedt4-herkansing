import React from 'react';
import {connect} from "react-redux";
import {setHeaderTitle} from "./Redux/Actions";
import axios from "axios";

class ProfielReserveringen extends React.Component{
    componentDidMount(){
        this.props.setHeaderTitle("Uw Reserveringen")
    }
    
    handleClick = (path) =>{
        this.props.history.push(path)
    } 
//functie klanten id
// functie die het rooster ophaalt 
// functie die tijdslot ophaalt
// functie die locatie ophaalt
// kan in 1 functie

    render(){
        return(
            <article className ="container profielReserveringen">
                {setHeaderTitle}
                <div className="profielReserveringen__container">
                    <table className="profielReserveringen__table">
                        <tr className="profielReserveringen__table__row">
                            <th className="profielReserveringen__table__header">Reservering</th>
                            <th className="profielReserveringen__table__header">Datum</th> 
                            <th className="profielReserveringen__table__header">Tijd</th>
                        </tr>
                        <tr>
                            <td className="profielReserveringen__table__data">Roof{/*klantreserveringwaar functie ophalen*/}</td>
                            <td className="profielReserveringen__table__data">10-04-2010{/*reserveringsdatum functie ophalen*/}</td>
                            <td className="profielReserveringen__table__data">13:00-13:30{/*reserveringstijd functie ophalen*/}</td>
                        </tr>
                    </table>
                </div>
                <section className="profielReserveringen__linkcontainer">
                    <a className="profielReserveringen__linkcontainer__link" onClick={() => this.handleClick("/Profiel")}>Terug naar profiel</a>  
                </section>
            </article>
        )
    }
}
const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle };
}
export default connect(mapStateToProps,{setHeaderTitle: setHeaderTitle}) (ProfielReserveringen);