import React from "react";
import ReactDOM from "react-dom";
import Header from "./Header";
import Home from "./Home";
import App from "./App";
import AppAdmin from "./OrderManagement/App";
import HeaderAdmin from "./OrderManagement/Header";
import OMNavigation from "./OrderManagement/Navigation";
import Rooster from "./OrderManagement/Rooster";
import BestellenMainHeader from "./BestellenComponents/BestellenMainHeader";
import { Provider } from "react-redux";
import { store } from "./Redux/Store"
import Router from "./Router";
import History from "./History"
import OMhistory from "./OrderManagement/History";
import axios from 'axios';




console.log(window.location.pathname)


if(window.location.pathname.startsWith('/admin/')){
    ReactDOM.render(<Provider store={store}><HeaderAdmin history={History} /></Provider>, document.querySelector('#header'));

    ReactDOM.render(<Provider store={store}><OMNavigation history={OMhistory} /></Provider>, document.querySelector('#nav-admin'));
    ReactDOM.render(<Provider store={store}><AppAdmin /></Provider>, document.querySelector('#root'));

}

else if(window.location.pathname === '/'){
    document.querySelector('body').className = "front";

        console.log(window.location.pathname )
        ReactDOM.render(<Provider store={store}><Home history={History} /></Provider>, document.querySelector('#root'));
}
else{
    document.querySelector('body').className = "front";
    // document.querySelector('main').className = "container";
    ReactDOM.render(<Provider store={store}>
        <BestellenMainHeader history={History}/>
        </Provider>, document.querySelector('#header'));
        console.log(window.location.pathname )
    ReactDOM.render(<Provider store={store}><App /></Provider>, document.querySelector('#root'));
}
