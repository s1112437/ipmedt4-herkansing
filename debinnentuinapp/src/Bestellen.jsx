import React from "react";

import BestellenHeader from './BestellenComponents/BestellenHeader';
import BestellenBevestigingsButton from './BestellenComponents/BestellenBevestigingsButton';
import MenuCategorieList from "./BestellenComponents/MenuCategorieList";
import MenuTheRoof from "./BestellenComponents/MenuTheRoof";
import BestellenModal from './BestellenComponents/BestellenModal';
import BestellenCartOverzicht from './BestellenComponents/BestellenCartOverzicht';
import { connect } from 'react-redux';
import { setHeaderTitle } from "./Redux/Actions";

import "./style/app.css";


class Bestellen extends React.Component{
    showLocationMenu = () => {
        if(this.props.location === 1){
            return <MenuCategorieList />;
        }
        if(this.props.location === 2){
            return <MenuTheRoof />;
        }
    }

    componentDidMount(){
        this.props.setHeaderTitle("Bestellen");
    }
    render(){
        console.log(this.props.location);
        return(
            <article className="container bestellen">
                <BestellenHeader />
                <BestellenCartOverzicht history={this.props.history}/>
                {this.showLocationMenu()}
                <BestellenModal history={this.props.history}/>
                
                <BestellenBevestigingsButton history={this.props.history}/>
            </article>
        );
    }
}

const mapStateToProps = state => {
    return { setHeaderTitle: state.setHeaderTitle, location: state.location};
}
export default connect(mapStateToProps,
    {setHeaderTitle: setHeaderTitle})(Bestellen);