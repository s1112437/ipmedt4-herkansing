import React from 'react';
import axios from 'axios';
import CommunityInvitesOM from './CommunityInvitesOM';
import { store } from "../Redux/Store";
import { Provider } from 'react-redux';

class OMCommunityInvites extends React.Component{

    state = {communityInvites: []};

    componentDidMount() {
        axios.get("http://localhost:8000/api/invitesRooster", {
            withCredentials: true
        }).then((res) => {
            res.data.forEach((communityInvites_info) => {
                this.setState({
                    communityInvites: [...this.state.communityInvites, communityInvites_info]
                });
            });

        })
    };

    render(){
        return (
            <article className="OM__container">
            <section className="OM__container OM__communityInvites">
                <header className="OM__container__header">
                    <h2>Community Invites</h2>
                    <a href="/admin/klanten/invites/create"  className="OM__container__header__button">
                    <span className="OM__container__header__button__icon material-icons">
                        add
                    </span>
                        Invite toevoegen
                    </a>
                </header>
                </section>
                <table  className="OM__communityInvites__table">
                        <thead>
                        <tr>
                            <th scope="col">E-mail</th>
                            <th scope="col">Verified</th>
                        </tr>

                        </thead>
                    <Provider store={store}>
                        <CommunityInvitesOM gotoEdit={(communityInvites) => this.props.history.push({
                            state: {communityInvites: communityInvites},
                        })} communityInvitesList={this.state.communityInvites} history={this.props.history}
                        />
                    </Provider>
                </table>
                </article>

        )
    }
}
export default (OMCommunityInvites)