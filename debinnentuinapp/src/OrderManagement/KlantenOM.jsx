import React from 'react';
import axios from 'axios';

import { store } from "../Redux/Store";
import { connect } from 'react-redux';


class KlantenOm extends React.Component{


    render(){
        //Hier wordt een klant uitgelezen  van de props uitgelzen en omgezet naar een tabel rij.
        const klantenList = this.props.klantenList.map(klanten => <tr key={klanten}>
            <td>{klanten.name}</td>
            <td>{klanten.email}</td>
            <td>{klanten.phone}</td>
            <td>{klanten.community_lid}</td>
            
            </tr>)
    return (
        <tbody>
            {/* tabelrij ingeladen */}
            {klantenList}
        </tbody>
        )   
    }
}
const mapStateToProps = state => {
    return {klanten: state.klanten}
}
export default connect(mapStateToProps, {})(KlantenOm);