import React from 'react';
import axios from 'axios';

class OMCommunityInviteCreate extends React.Component{
    state = {}
    
    handleOnSubmit = (e) => {
        e.preventDefault();
        axios.post("http://localhost:8000/api/inviteSend", this.state).then(res => {
            console.log(res)
        })
    }
    handleOnChange = (e) => {
        let target = e.target
        let value = target.value
        let name = target.name
        this.setState({
            [name]: value
        })
    }
    render(){
        return(
            <article className="OM__container">
                <section className="OM__container OM__communityInviteCreate">
                    <header className="OM__communityInviteCreate__header">
                        <h2>Community klant verificatie mail</h2>
                    </header>
                    <form onSubmit={this.handleOnSubmit} method="post">
                        <input className="OM__communityInviteCreate__input" onChange={this.handleOnChange} name="email" type="email" placeholder="Vul email in"></input>
                        <button className="OM__communityInviteCreate__button" type="submit" value="Submit">Verzenden</button>
                    </form>             
                </section>
            </article>
        )
    }
}
export default OMCommunityInviteCreate