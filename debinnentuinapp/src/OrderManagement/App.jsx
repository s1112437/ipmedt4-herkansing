import React from "react";
import Roosters from "./Roosters";
import RoosterCreate from "./RoosterCreate";
import RoosterEdit from "./RoosterEdit";
import Tijdsloten from "./Tijdsloten";
import Reserveringen from "./Reserveringen";
import MapEditor from "./mapEditor";
import ReserveringenMap from "./ReserveringenMap";
import ShowReservering from "./ShowReservering";
import ConfirmDeleteRooster from "./ConfirmDeleteRooster";
import CommunityInvites from "./CommunityInvites";
import CommunityInvitesCreate from "./CommunityInvitesCreate";
import AdminPrivateRoute from "./AdminPrivateRoute";
import Login from "./Login";
import Klanten from "./Klanten";
import BestellingenOM from "./BestellingenOM";
import BestellingenView from "./BestellingenView";
import { Link, Switch, Route, BrowserRouter as Router } from "react-router-dom";
import "../style/OrderManagement/appAdmin.css";
import { Provider, connect } from "react-redux";
import { store } from "../Redux/Store";
import history from "../History";
import {setAdminAuthenticated} from "../Redux/Actions";
import AXIOS from "../axios";

class App extends React.Component {
    componentDidMount(){
        this.checkAuthenticate();
    }

    checkAuthenticate = () =>  {
        AXIOS.get("http://localhost:8000/api/auth/" + 'check-authenticate').then((res) => {
            if(res.status === 200){
                AXIOS.get("http://localhost:8000/api/user/" + res.data.user.id).then(res => {
                if(res.data.User.role === "Admin"){
                        this.props.setAdminAuthenticated(true)
                    }
                });
            }
        }).catch(error => {if (error.response) {
            console.log("Unauthorized")
        }
    })
    }
    render(){
        return (
            <Router>
                <Switch>
                    <Route path="/admin/login" component={Login} history={history} />
                    
                    <AdminPrivateRoute path="/admin/reserveringen" component={Reserveringen} />

                    <AdminPrivateRoute path="/admin/rooster/create" component={RoosterCreate} />
                    <AdminPrivateRoute path="/admin/rooster/delete" component={ConfirmDeleteRooster} />

                    <Provider store={store}>
                    <AdminPrivateRoute path="/admin/tijden/tijdsloten" component={Tijdsloten}  />
                        <AdminPrivateRoute path="/admin/reservering" component={ShowReservering} history={history}  />

                        <AdminPrivateRoute path="/admin/reserveringenmap" component={ReserveringenMap}  />
                        <AdminPrivateRoute path="/admin/tijden/roosters"  history={history}  component={Roosters} />
                        <AdminPrivateRoute path="/admin/rooster/edit" component={RoosterEdit}  />
                        <AdminPrivateRoute path="/admin/reserveringen" history={history} component={Reserveringen}  />
                        <AdminPrivateRoute path="/admin/mapeditor" component={MapEditor}  />

                        <AdminPrivateRoute exact path="/admin/klanten" component={Klanten} />

                        <AdminPrivateRoute exact path="/admin/klanten/invites" component={CommunityInvites} />
                        <AdminPrivateRoute exact path="/admin/klanten/invites/create" component={CommunityInvitesCreate} />

                        <AdminPrivateRoute path="/admin/bestellingen/" component={BestellingenOM}  />
                        <AdminPrivateRoute path="/admin/bestelling/items" component={BestellingenView}  />
                    </Provider>


                </Switch>
            </Router>
        );
    }
}

export default connect(null, {setAdminAuthenticated: setAdminAuthenticated}) (App);