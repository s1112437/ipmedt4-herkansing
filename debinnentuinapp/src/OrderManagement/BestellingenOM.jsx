import React from 'react';
import axios from 'axios';
import BestellingOM from './BestellingOM.jsx';
import { Provider } from "react-redux";
import { store } from "../Redux/Store";

 class BestellingenOM extends React.Component {
    state = {bestellingen: []};

    // Alle bestellingen worden eenmaal uit de database gehaald en in de state gezet.
    componentDidMount() {
        axios.get("http://localhost:8000/api/bestelorders", {
            withCredentials: true
        }).then((res) => {
            res.data.forEach((bestelling_order) => {
                this.setState({
                    bestellingen: [...this.state.bestellingen, bestelling_order]
                }); 
            });
        })
    };

    render(){
        
    return(
        <article className="OM__container">
            <section className="OM__container OM__bestellingen">
                <header className="OM__container__header">
                    <h2>Bestellingen</h2>
                </header>
                <table className="OM__bestellingen__table">
                        <thead>
                            <tr>
                                <th scope="col">BestellingID</th>
                                <th scope="col">ReserveringID</th>
                                <th scope="col">Klant</th>
                                <th scope="col">Status</th>
                                <th scope="col">Totaal Prijs</th>
                                <th scope="col">Extra Info</th>
                                <th scope="col">Gemaakt in</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>

                        {/* Voor elke item in de bestelling state array maak een BestellingOM object (de rows van de tabel) states worden ook meegegeven als props*/}
                        <Provider store={store}>
                            <BestellingOM goToEdit={(bestellingen) => this.props.history.push({
                                pathname: '/admin/bestelling/items',
                                state:  {bestellingen: bestellingen},
                                })} bestellingenList={this.state.bestellingen} history={this.props.history} 
                            />
                        </Provider>
                </table>
            </section>
        </article>
        )
    }
}

export default BestellingenOM;