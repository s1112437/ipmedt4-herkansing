import React from 'react';
import moment from 'moment';
import {connect} from "react-redux";
import {setCurrentRooster} from "../Redux/Actions";
import { useHistory } from "react-router-dom";
import history from "./History";
class OMRooster extends React.Component {

    setCurrentRooster = roosterId => {

        
        this.props.setCurrentRooster(roosterId);
        this.props.goToEdit(roosterId);
        
    }
    deleteRooster = roosterId => {
        this.props.setCurrentRooster(roosterId);
        this.props.goToConfirmDelete(roosterId);
    }
    render(){
        const roosterList = this.props.roosterlist.map(rooster => <tr key={rooster}>
            <td>{rooster.name}</td>
            <td>{rooster.Locatie}</td>
            <td>{rooster.oneindig ? "Oneindig" : moment(rooster.startDatum).format("DD-MM-YYYY")}</td>
            <td>{rooster.oneindig ? "Oneindig" : moment(rooster.eindDatum).format("DD-MM-YYYY")}</td>
            <td className="OM__roosters__table__icon">
            <a onClick={() => this.setCurrentRooster(rooster.id)} className="material-icons">
    create
    </a>
    <a onClick={() => this.deleteRooster(rooster.id)} className="material-icons">
    delete
    </a>
    
            </td>
        </tr>)
        return (
        <tbody>
            {roosterList}
        </tbody>
        )
    }
}
const mapStateToProps = state => {
    return {currentRooster: state.currentRooster}
}
export default connect(mapStateToProps, {setCurrentRooster: setCurrentRooster}) (OMRooster);