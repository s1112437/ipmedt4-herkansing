import React from 'react';
import moment from 'moment';
import axios from 'axios';
import OMTijdslot from './Tijdslot';
import { connect } from 'react-redux';
import { Provider } from "react-redux";
import { store } from "../Redux/Store";

class OMTijdsloten extends React.Component{
    state = {date: moment().format(), tijdsloten: [], locations: [], selectedLocation: 1, showSuccessMsg: true};
    componentDidMount(){
        this.getTimeslots()
        this.getLocations()

    }
    getTimeslots = () => {
        console.log("test")
        this.setState({
            tijdsloten: []
        })
        axios.get("http://localhost:8000/api/admin/timeslots/" + moment(this.state.date).format('YYYYMMDD') + "/" + this.state.selectedLocation).then((res) => {
            this.setState({
                tijdsloten: res.data
            })    
    })
}
getLocations = () => {
    axios.get("http://localhost:8000/api/locations/").then((res) => {
        this.setState({
            locations: res.data
        })
    })
}
    addDay = (event) => {
        event.preventDefault();
        this.setState({
            date: moment(this.state.date).add(1,"days")
        }, () => {
            this.getTimeslots();
        });
        

    }
    removeDay = (event) => {
        event.preventDefault();
        this.setState({
            date: moment(this.state.date).add(-1,"days")
        }, () => {
            this.getTimeslots();
        });
        
    }
    selectLocation = (e) =>{
        console.log(e.target.value)
        this.setState({
            selectedLocation: e.target.value
        }, () => {
            this.getTimeslots();
        })
    }
    showSuccessMsg = (e) =>{
        console.log(e)
        this.setState({
            showSuccessMsg: false
        })
    }
    render(){
        
        return(
            <article className="OM__container">
                <section className="OM__container OM__tijdsloten">
                    <header className="OM__container__header">
                    <h2>Tijdsloten</h2>
                    <i className={this.props.message.Type === "Success" ? "OM__container__header__successMsg" : ""}>{this.props.message.Message}</i>
                    <article className="OM__container__header__filterContainer">
                        <label className="OM__container__header__filterContainer__label" for="location">Locatie:</label>
                        <select className="OM__container__header__filterContainer__input" id="location" name="location" value={this.state.selectedLocation} onChange={this.selectLocation}>
                            {this.state.locations.map(location => <option value={location.id} >{location.Locatie}</option>)}
                        </select>
                        <button onClick={this.removeDay} className="OM__container__header__filterContainer__prev material-icons">
                        navigate_before
                        </button>
                        <section className="OM__container__header__filterContainer__date ">
                        {moment(this.state.date).format("DD-MM-YYYY")}
                        </section>
                        <button onClick={this.addDay} className="OM__container__header__filterContainer__next material-icons">
                        navigate_next
                        </button>
                    </article>
                    </header>
                </section>
                <table className="OM__tijdsloten__table">
                <thead>
                        <tr>
                            <th scope="col">Starttijd</th>
                            <th scope="col">Eindtijd</th>
                            <th scope="col">Aantal personen</th>
                            <th scope="col">Beschikbaar</th>
                            <th scope="col"></th>

                        </tr>

                        </thead>
                        <Provider store={store}>
                        <OMTijdslot getTijdsloten={this.getTimeslots} showSuccessMsg={this.showSuccessMsg} tijdslotList={this.state.tijdsloten} />

                        </Provider>
                </table>
            </article>
        )
    }
}
const mapStateToProps = state => {
    return {message: state.message}
}
export default connect(mapStateToProps) (OMTijdsloten);