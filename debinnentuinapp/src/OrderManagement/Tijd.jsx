import React from 'react';
import Reservering from './Reservering';
import axios from 'axios';
import moment from 'moment';

class OMTijden extends React.Component{
    state = {reserveringen: []}
    componentDidMount() {
        this.getReserveringen(this.props.tijd)
    }
    getReserveringen(tijd) {
        axios.get("http://localhost:8000/api/reserveringen/" + tijd + "/" + moment(this.props.date).format() + "/" + this.props.selectedLocation).then((res) => {
            res.data.forEach(reservering => {
                this.setState({
                    reserveringen: [...this.state.reserveringen, Object.values(reservering)]
                })
            })
      
        })
        
    }
    render(){
        let reserveringenElms
        reserveringenElms = this.state.reserveringen.map(reservering => <Reservering history={this.props.history} reservering={reservering} />)
        return (
            <li className="OM__reserveringen__list__item">
                <section className="OM__reserveringen__list__item__time">{this.props.tijd}</section>
                <table cellspacing="0" cellpadding="0" className="OM__reserveringen__list__item__table">
                {reserveringenElms}
                </table>
            
            </li>
           
        )
    }
}

export default OMTijden;