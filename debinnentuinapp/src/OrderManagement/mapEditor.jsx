import React from 'react';
import ReactDOM from "react-dom";
import Selector from "./Selector";
import Editor from "./Editor";
import axios from "axios";
class OMmapEdit extends React.Component{
    state = {selectedTable: null, newTable: null, selectedLocation: 1, locations: [], tables: [], showSuccessMsg: true}
    componentDidMount(){
        this.getLocations();
        this.setState({
            editor:  new Editor(document.getElementById("js--stage")),
            selector: new Selector(document.getElementById("js--stage"))
        })
        this.getMap();
        this.getTables();
    }
    getLocations(){
        axios.get("http://localhost:8000/api/locations").then((res) => {
            this.setState({
                locations: res.data
            })
        })
    }
    getTables(){
        axios.get("http://localhost:8000/api/tablesbylocation/" + this.state.selectedLocation).then((res) => {
            this.setState({
                tables: res.data
            })
        })
    }
    selectLocation = (e) =>{
        console.log(e.target.value)
        this.setState({
            selectedLocation: e.target.value
        }, () => {
            this.getTables();
            this.getMap();
        })
    }
    changeSelectedTable = (e) => {
        this.setState({
            selectedTable: e.target.value
        })
    }
    parseNumber = ( value ) => {
        console.log(value)
        return parseFloat( value.toFixed( 2 ) );

    }
    setNewTable = (e) => {
        this.setState({
            newTable: e.target.value
        })
    }
    createTable = () => {
        if(this.state.selectedTable === null && this.state.newTable === null ){
            document.getElementById('js--message').style.display = "block"
        }
        else if (this.state.selectedTable !== null){
            const NS = 'http://www.w3.org/2000/svg';
            const WIDTH = 870;
            const HEIGHT = 600;
            // var element = React.createElement(
            //     "rect",
            //     {
            //     x: this.parseNumber( 0.4 * WIDTH ),
            //     y: this.parseNumber( 0.4 * HEIGHT ),
            //     width: this.parseNumber( 0.8 * 100 ) ,
            //     height: this.parseNumber( 0.8 * 100 ),
            //     style: { stroke: "black", fill: "green"},
            //     onClick: () => {this.selectTable(this.state.selectedTable)}
            //     }
            // )
            var element = document.createElementNS( NS, 'rect' );
            console.log(this.parseNumber( 0.4 * WIDTH ))
            element.setAttribute( 'text', "/Tafel.png");
            
            element.setAttribute( 'x', this.parseNumber( 0.4 * WIDTH ) );
            element.setAttribute( 'y', this.parseNumber( 0.4 * HEIGHT ) );
            element.setAttribute( 'width', this.parseNumber( 0.8 * 100 ) );
            element.setAttribute( 'height', this.parseNumber( 0.8 * 100 ) );
            element.style.stroke = 'black';
            element.setAttribute("class", "tables")
            element.style.fill =  "green";
            element.setAttribute("id", "js--table-" + this.state.selectedTable);
            console.log(document.getElementById("js--stage"));
            document.getElementById("js--stage").appendChild( element );
            // ReactDOM.render(element, document.getElementById('js--stage'));
            this.setState({
                selectedTable: null
            })
        }
        else if(this.state.newTable !== null){
            axios.post('http://localhost:8000/api/tables',{
                name: this.state.newTable,
                locationId: this.state.selectedLocation
            }).then((res) => {
                const NS = 'http://www.w3.org/2000/svg';
                const WIDTH = 870;
                const HEIGHT = 600;
                var element = document.createElementNS( NS, 'rect' );
                console.log(this.parseNumber( 0.4 * WIDTH ))
                element.setAttribute( 'src', "/Tafel.png");
                element.setAttribute( 'x', this.parseNumber( 0.4 * WIDTH ) );
                element.setAttribute( 'y', this.parseNumber( 0.4 * HEIGHT ) );
                element.setAttribute( 'width', this.parseNumber( 0.8 * 100 ) );
                element.setAttribute( 'height', this.parseNumber( 0.8 * 100 ) );
                element.setAttribute("class", "tables");
                element.style.stroke = 'black';
    
                element.style.fill =  "green";
                element.setAttribute("onClick", "this.selectTable(" + res.data + ")")
                element.setAttribute("id", "js--table-" + res.data);
                document.getElementById("js--stage").appendChild( element );
                this.setState({
                    newTable: null
                })
            });

        }
    }
    getMap = () => {
        axios.get("http://localhost:8000/api/maps/" + this.state.selectedLocation, ).then((res) => {
            if(res.data !== ""){
                // document.getElementById('js--content').innerHTML = res.data.map;
                // let svg = document.getElementById('js--content').childNodes[0];
                // svg.setAttribute('id','js--stage')
                this.state.editor.setSVG( new DOMParser().parseFromString( res.data.map, 'image/svg+xml' ) );

            }
            else{
                document.getElementById('js--stage').innerHTML = "";
            }
            // document.getElementById('js--content').childNodes[0].setAttribute("id","js--stage")
        })
    }
    save = (e) => {
            e.preventDefault();
			var link = document.createElement( 'a' );
			link.style.display = 'none';



                var blob = new Blob( [ this.state.editor.toString() ], { type: 'text/plain' } );
                const data = new FormData();
                data.append('locationId', this.state.selectedLocation);
                data.append('map', blob, "test");

                axios.post("http://localhost:8000/api/maps", data, {
                    headers: {
                        'Content-Type': 'multipart/form-data; boundary=${data._boundary}'
                      }
                }).then(() => {
                    this.setState({
                        showSuccessMsg: false
                    })
                })


    }
    render(){

        return (
            <article className="OM__container">
            <section className="OM__container">
                <header className="OM__container__header">
                    <h2>Mapeditor</h2>
                    <article className="OM__container__header__filterContainer">
                        
                    <label className="OM__container__header__filterContainer__label" htmlFor="location">Locatie:</label>
                        <select className="OM__container__header__filterContainer__input" id="location" name="location" value={this.state.selectedLocation} onChange={this.selectLocation}>
                            {this.state.locations.map(location => <option value={location.id} >{location.Locatie}</option>)}
                        </select>
                        </article>
                    <section hidden={this.state.showSuccessMsg} className="OM__container__header__successMsg">Opgeslagen!</section>
                    <a onClick={this.save} href=""  className="OM__container__header__button">
                    <span className="OM__container__header__button__icon material-icons">
                        save
                    </span>
                        Opslaan
                    </a>

                </header>
                </section>
                <article className="OM__mapEditor">
                    <section className="OM__mapEditor__tools">
                        <select name="selectedTable" value={this.state.selectedTable} onChange={this.changeSelectedTable}>
                            <option value={null}>Selecteer tafel</option>
                            {Array.from(this.state.tables).map(table => <option value={table.id} >{table.name}</option>)}

                        </select>
                        <input className="OM__mapEditor__tools__input" value={this.state.newTable} onChange={this.setNewTable} placeholder="nieuwe tafelnaam" name="newTable" type="text"></input>
                        <button className="OM__mapEditor__tools__button" onClick={this.createTable} >Tafel aanmaken</button>
                        <section className="OM__mapEditor__tools__message"  id="js--message">Geen tafel geselecteerd of opgegeven</section>
                    </section>
                    <section id="js--content" className="OM__mapEditor__content">
                        <svg id="js--stage" width="870" height="600" viewBox="0 0 870 600"></svg>
                    </section>
                </article>
                </article>
        )

    }
}

export default OMmapEdit