import React from 'react';
import axios from 'axios';
import moment from 'moment';

class OMReservering extends React.Component{
    state = {startTijd: null, eindTijd: null, Color: null, customer: null}
    componentDidMount(){
        
        this.getTime(this.props.reservering[3])
        
        let Status = this.props.reservering[8]
        if(Status === "Concept"){
            this.setState({
                Color: "blue"
            })
        }
        if(Status === "Alert"){
            this.setState({
                Color: "yellow"
            })
        }
        if(Status === "Canceled"){
            this.setState({
                Color: "red"
            })
        }
        if(Status === "Accepted"){
            this.setState({
                Color: "green"
            })
        }
        this.getCustomer();
    }
    getTime(tijdslotId) {
        axios.get("http://localhost:8000/api/timeslot/" + tijdslotId).then((res)=> {
            console.log( res)
            this.setState({
                startTijd: moment(res.data.Tijdslot.startTijd, "HH:mm:ss").format("HH:mm")
            })
        });
    }
    gotoReservation = (reserveringsId) => {
        console.log(this.props)
        this.props.history.push({
            pathname: '/admin/reservering',
            state: { reserveringId: reserveringsId }
        })
    }
    getCustomer = () => {
        axios.get("http://localhost:8000/api/klant/" + this.props.reservering[5]).then(res => {
            this.setState({
                customerName: res.data.name
            })
            console.log(this.state)
        })
    }
    render(){
        
        return (
            <tr onClick={() => this.gotoReservation(this.props.reservering[0])} className={"OM__reserveringen__list__item__table__row OM__reserveringen__list__item__table__row" + "--" + this.state.Color}>
                <td className="OM__reserveringen__list__item__table__row__column"></td>
                <td className="OM__reserveringen__list__item__table__row__column OM__reserveringen__list__item__table__row__column--time"><section className="OM__reserveringen__list__item__table__row__column__time">{this.state.startTijd}</section></td>
                <td className="OM__reserveringen__list__item__table__row__column OM__reserveringen__list__item__table__row__column--name">{this.state.customerName}</td>
                <td className="OM__reserveringen__list__item__table__row__column">{this.props.reservering[4]}p</td>
            </tr>
        )
    }


}

export default OMReservering;