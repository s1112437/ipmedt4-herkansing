import React from "react";
import Reserveren from "./Reserveren";
import Login from "./Login";
import Register from "./Register";
import Profiel from "./Profiel";
import ProfielReserveringen from "./ProfielReserveringen";
import WachtwoordHerstel from "./WachtwoordHerstel";
import WachtwoordReset from "./WachtwoordReset";
import { Link, Switch, Route, BrowserRouter as Router, withRouter  } from "react-router-dom";
import WachtwoordWijzigen from "./WachtwoordWijzigen";
import Home from "./Home";
import AdminApp from "./OrderManagement/App";
import Bestelvalidatie from "./Bestelvalidatie";
import Header from "./Header";
import "./style/app.css";
import { Provider, connect } from "react-redux";
import { setAuthenticated, setCustomer } from "./Redux/Actions"
import { store } from "./Redux/Store"
import  history from "./History";
import PrivateRoute from "./Router/PrivateRoute";
import AXIOS from "./axios";
import Bestellen from "./Bestellen";
import BestellenOverzicht from "./BestellenOverzicht";
import Betaling from "./Betaling";
import SuccesfullPayment from "./SuccesfullPayment"
class App extends React.Component {
    componentDidMount(){
        this.checkAuthenticate();
    }
    setBackground() {
        if(this.props.location === 2){
            document.body.className = "front front--theroof"
        }
        else {
            document.body.className = "front"
        }

    }
    checkAuthenticate = () =>  {
        AXIOS.get("http://localhost:8000/api/auth/" + 'check-authenticate').then((res) => {
            if(res.status === 200){
                this.props.setAuthenticated(true)
                console.log("test")
                AXIOS.get("http://localhost:8000/api/user/" + res.data.user.id).then(res => {
                    AXIOS.get(res.data.Klant).then(res => {
                        this.props.setCustomer(res.data);
                    })
                });
            }
        }).catch(error => {if (error.response) {
            console.log("Unauthorized")
        }
    })
    }
    render(){
        this.setBackground();

        return (

            <Router>
                <Switch>
                    <Provider store={store}>
                    <Route path="/home" component={Home} history={history} />
                    <article id="js--container" className="container">
                        <Route path="/Login" customer={this.props.customer} isAuthenticated={() => this.state.isAuthenticated} component={Login} history={history} />
                        <Route path="/Register" component={Register} history={history} />   
                        <Route path="/registerCommunity/:token" component={Register} history={history} />   
                        <PrivateRoute path="/profiel" customer={this.props.customer} authed={() => this.state.isAuthenticated} component={Profiel} />                         <Route path="/ProfielReserveringen" component={ProfielReserveringen} history={history} /> 
                        <Route path="/WachtwoordHerstel" component={WachtwoordHerstel} history={history} />     
                        <Route path="/WachtwoordReset/:token" component={WachtwoordReset} history={history} />
                        <Route path="/reserveren" component={Reserveren} history={history}/>
                        <Route path="/bestellen" component={Bestellen} history={history} />
                        <Route path="/WachtwoordWijzigen" component={WachtwoordWijzigen} history={history} />
                        <Route path="/besteloverzicht" component={BestellenOverzicht} history={history} />
                        <Route path="/betaling" component={Betaling} history={history} />
                        <Route path= "/succesfullpayment" component={SuccesfullPayment} history={history} />
                        <Route path="/bestelvalidatie" component={Bestelvalidatie} history={history} />
                    </article>

                    </Provider>

                </Switch>
            </Router>
        );
    }
}
const mapStateToProps = state => {
    return { location: state.location,
        isAuthenticated: state.isAuthenticated,
    customer: state.customer };
}
export default connect(mapStateToProps, {setAuthenticated: setAuthenticated, setCustomer: setCustomer}) (App);
